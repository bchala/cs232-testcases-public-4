class CallInherited {
  public static void main(String[] a){
      System.out.println(new D().m(10));
  }
}

class D {
  public int m(int i){
    int j;
    if (5 < i) 
       j = new L().f();
    else 
       j = 5;
    return j;
  }
}

class K {
  public int f() {
    return 2;
  }  
  public int g() {
    return 3;
  }
}

class L extends K {
  public int f() {
    return this.g();
  }  
}

class M extends L {
  public int g() {
    return 9;  // 9 ~ "no" in German
  }  
}
