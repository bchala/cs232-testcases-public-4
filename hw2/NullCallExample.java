// this I/O pair is an example of both a valid pointer and pointers to a null/uninitialized object

class NullCallExample {
    public static void main(String[] a){
        System.out.println(new A().invoke());
    }
}

class A {
	B b;
	C c;
	int bnum;
	int cnum1;
	int cnum2;
	
	public int invoke() {
		int intermediate; 
		c = new C();
		bnum = b.foo();
		cnum1 = c.bar();
		cnum2 = c.fb(b);
		intermediate = bnum + cnum1;
		return intermediate + cnum2;
	}
}

class B { 
	public int foo() {
		return 4;
	}
	
	public int gee() {
		return 8;
	}
}

class C { 
	public int bar() {
		return 29;
	}
	
	public int fb(B b) {
		return b.gee();
	}
}